<?php

namespace GraphQLFrontApi;
use Exception;
use Netpromotion\Profiler\Profiler;
use Nette\{Caching\IStorage};
use Tracy\Debugger;

/**
 * Class BaseBackend
 * Base class for API
 * @author Martin Pavelka
 * @package App\Models\API\Backend
 */
class GraphQLCommunication {

    /** @var Logger $logger - logger service */
    protected $logger;

    /** @var GraphQLCreator $graphQLCreator - graphQL creator service */
    protected $graphQLCreator;

    /** @var string $domain - curl endpoint */
    private $domain;

    /** @var IStorage|null $cache - cache interface */
    protected $cache;

    /** @var string $accessToken - access token */
    protected $accessToken;

    /** @var int $apiTimeout - timeout for curl request */
    private $apiTimeout;

    /** @var int $env - environment 0 - develop */
    private $env;

    /**
     * Sets the access token
     * @param string $accessToken - the access token
     */
    public function setAccessToken(string $accessToken): void {
        $this->accessToken = $accessToken;
    }

    /**
     * BaseBackend constructor.
     * @param string $apiUrl - api endpoint
     * @param int $apiTimeout - curl timeout
     * @param int $env - 0 for develop
     * @param Logger $logger - logger service
     * @param GraphQLCreator $graphQLCreator - graphQL service
     * @param IStorage|null $cache - cache interface
     */
    public function __construct(string $apiUrl, int $apiTimeout, int $env, Logger $logger,
        GraphQLCreator $graphQLCreator, ?IStorage $cache) {

        $this->graphQLCreator = $graphQLCreator;
        $this->domain = $apiUrl;
        $this->cache = $cache;
        $this->logger = $logger;
        $this->apiTimeout = $apiTimeout;
        $this->env = $env;
    }

    /**
     * Send the request to the API
     * @param array|null $query - something like [query => "queryString"]
     * @param string $type - the request type POST, GET, DELETE, PUT
     * @return array - the result decoded from the JSON
     * @throws ErrorApiException
     * @throws WarningApiException
     * @throws UnauthorizedException
     */
    public function sendRequestGraphQuery(? array $query, string $type) : array {

        // Init used variables
        $callerFunction = debug_backtrace()[1]['function'];
        $encodedJsonQuery = json_encode($query);
        $profilerKey = "sendRequestGraphQuery-$callerFunction-$type";

        // Init profiler
        if ($this->env === 0) Profiler::start($profilerKey);

        // Init curl
        $oCurlInstance = curl_init($this->domain);
        curl_setopt($oCurlInstance, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($oCurlInstance, CURLOPT_CONNECTTIMEOUT ,$this->apiTimeout);
        curl_setopt($oCurlInstance, CURLOPT_TIMEOUT, $this->apiTimeout);

        // If any input data
        if ($type === 'POST' || $type === 'PUT') {

            // Set header
            curl_setopt($oCurlInstance, CURLOPT_POSTFIELDS, $encodedJsonQuery);
            curl_setopt($oCurlInstance, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Authorization: Bearer ' . $this->accessToken,
                    'Content-Length: ' . strlen($encodedJsonQuery))
            );
        }

        // We don't have POST
        else {
            curl_setopt($oCurlInstance, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Authorization: Bearer ' . $this->accessToken
            ]);
        }

        // Run the cURL and check
        curl_setopt($oCurlInstance, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($oCurlInstance);
        $code = curl_getinfo($oCurlInstance, CURLINFO_HTTP_CODE);

        // If we have internal error
        if ($code === 500 || $result === false) {

            $this->logger->generateCurlLog($code, func_get_args(), $this->domain, $this->accessToken,
                null, false, $encodedJsonQuery);
            if ($this->env === 0) Profiler::finish($profilerKey);
            throw new ErrorApiException($code, func_get_args(), $this->domain, $this->accessToken);
        }

        // If we have unauthorized error
        if ($code === 403) {
            $this->logger->generateCurlLog($code, func_get_args(), $this->domain, $this->accessToken,
                json_encode($result), false, $encodedJsonQuery);
            if ($this->env === 0) Profiler::finish($profilerKey);
            throw new UnauthorizedException($code, func_get_args(), $this->domain, $this->accessToken);
        }

        // Decode
        $result = json_decode($result, true);

        // If we have some error
        if (!isset($result['data']) || $result['data'] === null || isset($result['data']['errors'])) {
            $this->logger->generateCurlLog($code, func_get_args(), $this->domain, $this->accessToken,
                json_encode($result), false, $encodedJsonQuery);
            if ($this->env === 0) Profiler::finish($profilerKey);
            throw new WarningApiException($code, func_get_args(), $this->domain, $this->accessToken, json_encode($result));
        }

        // Return the result
        $this->logger->generateCurlLog($code, func_get_args(), $this->domain, $this->accessToken,
            json_encode($result), true, $encodedJsonQuery);
        if ($this->env === 0) Profiler::finish($profilerKey);
        return $result['data'];
    }


    /**
     * Generate the cache key
     * @param string $class - the class caller
     * @param string $function - the caller function
     * @param string $additional - the parameters
     * @return string output key for cache
     */
    protected function generateCacheKey(string $class, string $function, string $additional) {
        return $this->env . '-' . $class . '-' . $function . '-' . $additional;
    }
}

/**
 * Class ErrorApiException
 * When the backend throws 500
 * @package App\Models\API
 */
class ErrorApiException extends Exception {

    /** @var array $callParameters - function parameters for the API function call */
    private $callParameters;

    /** @var string $endPoint - server name */
    private $endPoint;

    /** @var string $token - access token used */
    private $token;

    /**
     * ErrorApiException constructor.
     * @param int $httpCode - http code
     * @param array $callParameters - function parameters
     * @param string $endPoint - server
     * @param string $token - token
     * @param Exception|null $prev - previous exception
     */
    public function __construct(int $httpCode, array $callParameters, string $endPoint, ? string $token,
                                Exception $prev = null, string $exceptionTitle = "API Internal Error!") {

        parent::__construct($exceptionTitle, $httpCode, $prev);
        $this->callParameters = $callParameters;
        $this->endPoint = $endPoint;
        $this->token = $token;
    }

    /** @return array - function call parameters */
    public function getCallParameters(): array {
        return $this->callParameters;
    }

    /** @return string - server url */
    public function getEndPoint(): string {
        return $this->endPoint;
    }

    /** @return string - used token */
    public function getToken(): string {
        return $this->token;
    }
}

/**
 * Class WarningApiException
 * When the backend return some error
 * @package App\Models\API
 */
class WarningApiException extends ErrorApiException {

    /** @var array $curlResult - result returned from the API */
    private $curlResult;

    /**
     * WarningApiException constructor.
     * @param int $httpCode - http code
     * @param array $callParameters - function parameters
     * @param string $endPoint - server
     * @param string $token - token
     * @param array $curlResult - result from the API call
     * @param Exception|null $prev - prev exception
     */
    public function __construct(int $httpCode, array $callParameters, string $endPoint, ? string $token,
                                string $curlResult, Exception $prev = null) {

        parent::__construct($httpCode, $callParameters, $endPoint, $token, $prev, "API Warning Error!");
        $this->curlResult = $curlResult;
    }

    /** @return array - the result from the curl call */
    public function getCurlResult(): array {
        return $this->curlResult;
    }
}

/**
 * Class UnauthorizedException
 * @package GraphQLFrontApi
 */
class UnauthorizedException extends ErrorApiException {

    /**
     * UnauthorizedException constructor.
     * @param int $httpCode - http code
     * @param array $callParameters - function parameters
     * @param string $endPoint - server
     * @param string $token - token
     * @param array $curlResult - result from the API call
     * @param Exception|null $prev - prev exception
     */
    public function __construct(int $httpCode, array $callParameters, string $endPoint, ? string $token,
                                Exception $prev = null) {
        parent::__construct($httpCode, $callParameters, $endPoint, $token, $prev, "API Auth Error!");
    }
}