<?php

namespace GraphQLFrontApi\DI;
use Nette\DI\Compiler;
use Nette\DI\CompilerExtension;

class GraphQLFrontApiExtension extends CompilerExtension {

    private $defaults = [
        'apiUrl' => "",
        'loggerDir' => "API"
    ];

    public function loadConfiguration()
    {

        $config = $this->getConfig();
        $builder = $this->getContainerBuilder();

        $builder->addDefinition($this->prefix('slack'))
            ->setClass('GraphQLFrontApi\Slack', [$config['slackHook']]);

        $builder->addDefinition($this->prefix('logger'))
            ->setClass('GraphQLFrontApi\Logger', [$config['loggerDir'], $config['env'], $config['project']]);

        $builder->addDefinition($this->prefix('communication'))
            ->setClass('GraphQLFrontApi\GraphQLCommunication', [$config['apiUrl'], $config['apiTimeout'], $config['env']]);

        $builder->addDefinition($this->prefix('creator'))
            ->setClass('GraphQLFrontApi\GraphQLCreator', []);
    }
}