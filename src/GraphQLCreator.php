<?php

namespace GraphQLFrontApi;

/**
 * Class GraphQLCreator
 * @author Martin Pavelka
 * @version 1.1
 * @package App\Models\System
 */
class GraphQLCreator {

    /** @var string - prefix used when selecting properties */
    const selectedParameterPrefix = 'select';

    /** @var string - prefix for the entity name to be used in the query */
    const usedParameterPrefix = 'usedName';

    /**
     * Get the object properties which contains the selected prefix string
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param string $objectParameter - object property name
     * @return bool - true if selected prefix found
     */
    private static function getCalledSelected(string $objectParameter) : bool {
        return strpos($objectParameter, self::selectedParameterPrefix) !== false;
    }

    /**
     * Remove prefix from the selected parameter name and convert to camel case
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param string $selectPrefixedParameterName - original property name
     * @return string - updated name without selected and camelCase
     */
    private static function removeSelectedPrefix(string $selectPrefixedParameterName) {
        return self::camelCase(substr($selectPrefixedParameterName, strlen (self::selectedParameterPrefix)));
    }

    /**
     * Get params for the filtering (without select prefix and without null
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param $filterParameterValue - property value
     * @param $filterParameterName - property name
     * @return bool - true if is filtering one
     */
    private static function getFilterParams($filterParameterValue, $filterParameterName) {
        return strpos($filterParameterName, self::selectedParameterPrefix) === false && $filterParameterValue !== null;
    }

    /**
     * Convert string to camel case
     * @param string $str - normal string
     * @param array $noStrip
     * @return string - camelCase string
     */
    public static function camelCase(string $str, array $noStrip = []) : string {

        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);$str = ucwords($str);$str = str_replace(" ", "", $str);$str = lcfirst($str);
        return $str;
    }

    /**
     * Select all object parameters in the producing object
     * @noinspection PhpUndefinedClassInspection
     * @param object $resourceObject - resource object to be updated
     */
    public function selectAll(object $resourceObject) : void {

        $objParameters = $resourceObject->getObjectProps();
        $selectParameters = array_filter($objParameters, "self::getCalledSelected", ARRAY_FILTER_USE_KEY);
        array_walk($selectParameters, function(/** @noinspection PhpUnusedParameterInspection */
            &$x, $key) use ($resourceObject) {$resourceObject->$key = true;});
    }

    /**
     * Get which parameters we want to select
     * @noinspection PhpUndefinedClassInspection
     * @param object $resource
     * @return array - array of selecting ones
     */
    private function getSelectingParameters(object $resource) : array {

        $objParameters = $resource->getObjectProps();
        $selectParameters = array_filter($objParameters, "self::getCalledSelected", ARRAY_FILTER_USE_KEY);
        $justSelected = array_filter($selectParameters, function($key) use ($objParameters) {
            return $objParameters[$key] === true;}, ARRAY_FILTER_USE_KEY);
        return array_map("self::removeSelectedPrefix", array_keys($justSelected));
    }

    /**
     * Get the filtering parameters
     * @noinspection PhpUndefinedClassInspection
     * @param object $resource
     * @return array - filtering parameters array
     */
    private function getFilteringParameters(object $resource) : array {

        $objParameters = $resource->getObjectProps();
        $filterParameters = array_filter($objParameters, "self::getFilterParams", ARRAY_FILTER_USE_BOTH);
        return $filterParameters;
    }

    /**
     * Produce the query for the filtering
     * @param array $filteringParameters - array of filtering parameters
     * @param bool $isFilter - if is mutation
     * @todo filter parameter of object not just for id
     * @example (fromTime: "2018-01-01T00:00",toTime: "2018-01-31T23:59",userID: 172)
     * @return string - the filtering part of the query
     */
    private function produceFilterQuery(array $filteringParameters, bool $isFilter) : string {

        // Nothing we can finish if sth use the (
        if (empty($filteringParameters)) return "";
        $outputString = (!$isFilter) ? "(" : "(filter:{";

        // Now process all props
        foreach ($filteringParameters as $filteringParameterKey => $filteringParameterValue) {

            // We have used - this will be skipped
            if (strpos($filteringParameterKey, self::usedParameterPrefix) !== false)
                continue;

            // We have object - this should be reviewed by @todo please
            if (is_object($filteringParameterValue) && isset($filteringParameterValue->id)
                && $filteringParameterValue->id !== null) {
                $filterParameterName = self::camelCase($this->getJustClassName($filteringParameterValue)) . 'Id';
                $outputString .= "$filterParameterName: $filteringParameterValue->id,";
                continue;
            }

            // Others basic props
            if (is_bool($filteringParameterValue))  {
                $filteringParameterValueBool = ($filteringParameterValue) ? "true" : "false";
                $outputString .= "$filteringParameterKey: $filteringParameterValueBool,";
            } else if (is_integer($filteringParameterValue))
                $outputString .= "$filteringParameterKey: $filteringParameterValue,";
            else
                $outputString .= "$filteringParameterKey: \"$filteringParameterValue\",";
        }

        // Just finish query
        return (!$isFilter) ? rtrim($outputString, ',') . ")" : rtrim($outputString, ',') . "}, pageRequest: {})";
    }

    /**
     * Produce the query for the selecting
     * @noinspection PhpUndefinedClassInspection
     * @param array $selectingParameters - selecting parameters
     * @param object $resourceObject
     * @return string - selecting query
     */
    private function produceSelectQuery(array $selectingParameters, object $resourceObject) : string {

        // Nothing we can finish if sth use the (
        if (empty($selectingParameters)) return "";
        $outputString = "{";

        // Now process all props
        foreach ($selectingParameters as $selectingParameter) {

            // We have used - this will be skipped
            if (strpos($selectingParameter, 'usedName') !== false)
                continue;

            // Array -> need to be just object
            $temporaryParameterValue = null;
            if (is_array($resourceObject->$selectingParameter) && !empty($resourceObject->$selectingParameter))
                $temporaryParameterValue = current($resourceObject->$selectingParameter);

            // Just object
            if (is_object($resourceObject->$selectingParameter))
                $temporaryParameterValue = $resourceObject->$selectingParameter;

            // Process
            if ($temporaryParameterValue !== null) {
                $outputString .= $temporaryParameterValue->getQueryFromObjectWithForceName(
                    $temporaryParameterValue, $selectingParameter);
                continue;
            }

            // Append
            $outputString .= "$selectingParameter ";
        }

        // Just close the query
        return rtrim($outputString, ' ') . "}";
    }

    /**
     * Produce query for the object
     * @noinspection PhpUndefinedClassInspection
     * @param object $resourceObject
     * @param bool $isMutation
     * @param bool $isFilter
     * @return array - [query => "producedQuery]
     */
    public function getQueryFromObject(object $resourceObject, bool $isMutation = false, bool $isFilter = false) {

        $selectingParameters = $this->getSelectingParameters($resourceObject);
        $filteringParameters = $this->getFilteringParameters($resourceObject);
        $typeName = ($isMutation) ? "mutation" : "query";
        $prefix = "$typeName { ";
        $suffix = "}";
        return [ "query" =>
            $prefix . self::camelCase($resourceObject->usedNameInQuery)
            . $this->produceFilterQuery($filteringParameters, $isFilter)
            . $this->produceSelectQuery($selectingParameters,$resourceObject)
            . $suffix];
    }

    /**
     * Ups, redundant but used by recursion in filtering :(
     * @noinspection PhpUndefinedClassInspection
     * @param object $resourceObject
     * @param string $name - name of the prop used for the name
     * @return string - sub query here
     */
    public function getQueryFromObjectWithForceName(object $resourceObject, string $name) {

        $selectingParameters = $this->getSelectingParameters($resourceObject);
        $filteringParameters = $this->getFilteringParameters($resourceObject);
        $entityName = $name;
        return $entityName
            . $this->produceFilterQuery($filteringParameters)
            . $this->produceSelectQuery($selectingParameters,$resourceObject);
    }

    /**
     * Uff, get_class prepend the namespace but we don't want to do this
     * @noinspection PhpUndefinedClassInspection
     * @param object $resource
     * @return string - just class name
     */
    private function getJustClassName(object $resource) {
        $explodedEntityName = explode('\\', get_class($resource));
        return array_pop($explodedEntityName);
    }
}