<?php

namespace GraphQLFrontApi;
use Exception;

/**
 * Class BaseGraphQLEntity
 * @author Martin Pavelka
 * @version 1.0.1
 * @package App\Models\Backend\MainModule\Entity
 */
abstract class BaseGraphQLEntity {

    /** @var string - prefix for selecting prop */
    const selectedParameterPrefix = 'select';

    /** @var string - prefix for use prop */
    const useParameterPrefix = 'use';

    /** @var string $usedNameInQuery - use this name in query  */
    public $usedNameInQuery;

    /** @var GraphQLCreator $graphQLCreator */
    protected $graphQLCreator;

    /**
     * Construct object from the result
     * @param array $inputArrayFromJson
     */
    abstract public function constructFromInputArray(array $inputArrayFromJson) : void;

    /**
     * Just setter for the name used in the query
     * @param string $usedNameInQuery
     */
    public function setUsedNameInQuery(string $usedNameInQuery): void {
        $this->usedNameInQuery = $usedNameInQuery;
    }

    /**
     * Get the query from the object
     * @param bool $isMutation
     * @param bool $isFilter
     * @return array - [query => "producedQuery"]
     */
    public function getQueryFromObject(bool $isMutation = false, bool $isFilter = false) : array {
        return $this->graphQLCreator->getQueryFromObject($this, $isMutation, $isFilter);
    }

    /**
     * Get prop without trash
     * @return array - list of the props with the values
     */
    public function getObjectProps() : array {

        $objectProperties = get_object_vars($this);
        unset($objectProperties['graphQLCreator']);
        unset($objectProperties['usedNameInQuery']);
        return $objectProperties;
    }

    /**
     * Generate functions for use and select
     * @noinspection PhpUndefinedClassInspection
     * @param $func - calling function name
     * @param $params - array of params
     * @return $this|null - current object or null
     */
    function __call(string $func, array $params) : ? object {

        // Find selected
        $objectProperties = get_object_vars($this);
        $selectParameters = array_keys(array_filter($objectProperties, function(string $objectParameter) {
            return strpos($objectParameter, self::selectedParameterPrefix) !== false;}, ARRAY_FILTER_USE_KEY));

        // Find selected
        $baseParameters = array_keys(array_filter($objectProperties, function(string $objectParameter) {
            return strpos($objectParameter, self::selectedParameterPrefix) === false;}, ARRAY_FILTER_USE_KEY));

        // If selected set to true
        if (in_array($func, $selectParameters)) {
            $this->$func = true;
            return $this;
        }

        // If use set the prop
        if (strpos($func, "use") !== false) {
            $nameWithoutUse = $this->graphQLCreator::camelCase(substr($func, 3));
            if (in_array($nameWithoutUse, $baseParameters)) {
                $this->$nameWithoutUse = current($params);
                return $this;
            }
        }
        return null;
    }

    /** Init the props with the null and selects with the false */
    protected function initProps() : void {

        $objectProperties = $this->getObjectProps();
        array_walk($objectProperties, function(
            /** @noinspection PhpUnusedParameterInspection */$value, $key) {
            $selectPropertyName = $this->graphQLCreator::camelCase(self::selectedParameterPrefix . '-' . $key);
            $this->{$selectPropertyName} = false;
            $this->$key = null;
        });
    }

    /**
     * Get the object properties which contains the selected prefix string
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param string $objectParameter - object property name
     * @return bool - true if selected prefix found
     */
    private static function getCalledSelected(string $objectParameter) : bool {
        return strpos($objectParameter, self::selectedParameterPrefix) !== false;
    }

    /**
     * Remove prefix from the selected parameter name and convert to camel case
     * @noinspection PhpUnusedPrivateMethodInspection
     * @param string $selectPrefixedParameterName - original property name
     * @return string - updated name without selected and camelCase
     */
    private static function removeSelectedPrefix(string $selectPrefixedParameterName) {
        return self::camelCase(substr($selectPrefixedParameterName, strlen (self::selectedParameterPrefix)));
    }

    /**
     * Convert string to camel case
     * @param string $str - normal string
     * @param array $noStrip
     * @return string - camelCase string
     */
    public static function camelCase(string $str, array $noStrip = []) : string {

        $str = preg_replace('/[^a-z0-9' . implode("", $noStrip) . ']+/i', ' ', $str);
        $str = trim($str);$str = ucwords($str);$str = str_replace(" ", "", $str);$str = lcfirst($str);
        return $str;
    }

    /**
     * Get just raw parameters
     * @noinspection PhpUndefinedClassInspection
     * @param object $resource
     * @return array - array of selecting ones
     */
    protected function getEntityParameters() : array {

        $objParameters = $this->getObjectProps();
        $selectParameters = array_filter($objParameters, "self::getCalledSelected", ARRAY_FILTER_USE_KEY);
        return array_map("self::removeSelectedPrefix", array_keys($selectParameters));
    }

    /**
     * Get output as result array
     * @return array
     */
    public function getResultAsArray() {

        $outputArray = [];
        foreach ($this->getEntityParameters() as $parameter) $outputArray[$parameter] = $this->$parameter;
        return $outputArray;
    }
}

/**
 * Class RequitedParameterException
 * @package App\Models\Backend\MainModule\Entity
 */
class RequitedParameterException extends Exception {}

/**
 * Class EntityNotFoundInTheResultArray
 * @package App\Models\Backend\MainModule\Entity
 */
class EntityNotFoundInTheResultArray extends Exception {}